#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Example_1
:FILE: __main__.py
:AUTHOR: meddy boukhedouma
:CONTACT: meddy.boukhedouma@gmail.com
"""


from collections import namedtuple
import random

#SURFACE 
HEIGHT_SURFACE_MIN = 0
HEIGHT_SURFACE_MAX = 1000
WIDTH_SURFACE_MIN = 0
WIDTH_SURFACE_MAX = 1000

# TOTAL POINTS SCATTER IN THE SURFACE
TOTAL_SHAPE_PTS = 1000

# TOTAL POINTS CLOUD IN THE SURFACE
TOTAL_TARGET_PTS = 1000

# SHAPE LIST NAME with PARAMETER ISOMETRIC IF YOU WANT KEEP THE SYMETRIC IN x.size.value AND y.size.value
SHAPE_LIST = [
    {'type':'Circle{}', "isometric" : True},
    {'type':'Rectangle{}', "isometric" : False},
    {'type':'Diamond{}', "isometric" : False},
    ]

#GEO SHAPE SIZE MIN AND MAX
MIN_SIZE_SHAPE = 10
MAX_SIZE_SHAPE = 100

class Surface:
    """
    Object Surface with WIDTH_SURFACE_MAX, HEIGHT_SURFACE_MAX, TOTAL_PTS and data_ramdomized.
    """
    def __init__(self):
        self.height = HEIGHT_SURFACE_MAX
        self.width = WIDTH_SURFACE_MAX
        self.total_pts_shape = TOTAL_SHAPE_PTS
        self.total_pts_target = TOTAL_TARGET_PTS
        
        # random pivot shape points and target points
        self.point_shapes = self.random_point(nb_pts=self.total_pts_shape)
        self.points_target = self.random_point(nb_pts=self.total_pts_shape)
        self.shape_list = self.get_shape_list()
        
    def random_point(self ,nb_pts, only_point=False):
        """
        Set a list of point(x,y) or return one point(x,y)

        Args:
            nb_pts (int): nb point to randomize
            only_point (bool, optional): if you need only one point (x,y) or list of point[(x1,y1),(x2,y2),]. Defaults to False.

        Returns:
            list[namedtuple]: Returns list of namedtuple point(x,y) or one namedtuple point(x,y)
        """
        list_random_pts = {}
        PointRandomized = namedtuple('point', [ 'x', 'y' ])
        if only_point:
            # random coordinates pts
            x = random.SystemRandom().uniform(HEIGHT_SURFACE_MIN, HEIGHT_SURFACE_MAX)
            y = random.SystemRandom().uniform(WIDTH_SURFACE_MIN, WIDTH_SURFACE_MAX)
            pn = PointRandomized(x, y)
            return pn
        else:   
            for id_point in range(nb_pts):
                # random coordinates pts
                x = random.SystemRandom().uniform(HEIGHT_SURFACE_MIN, HEIGHT_SURFACE_MAX)
                y = random.SystemRandom().uniform(WIDTH_SURFACE_MIN, WIDTH_SURFACE_MAX)
                pn = PointRandomized(x, y)
                # create pivot pts from ramdom x and y point value
                list_random_pts[id_point] = pn         
            return list_random_pts
        
    def get_shape_list(self):
        """
        Set point for each point in list 'self.total_pts_shape' with Object Rectangle,Circle,Diamond.

        Returns:
            List: List Object Instance of Rectangle, Circle or Diamond.
        """
        shape_objects = []
        SizeRandomized = namedtuple('size',['width', 'height'])
            
        for shape_point in self.point_shapes:
             
            # get id_shape and coord id_shape
            id_pts = shape_point
            coord_pivot_pts = self.point_shapes[id_pts]
            
            # get random shape
            geoshape = random.choice(SHAPE_LIST)
            
            # Create Data shape pts
            random_shape_size = SizeRandomized(random.SystemRandom().uniform(MIN_SIZE_SHAPE, MAX_SIZE_SHAPE), random.SystemRandom().uniform(MIN_SIZE_SHAPE, MAX_SIZE_SHAPE))
            
            
            while (coord_pivot_pts.x + random_shape_size.width) > WIDTH_SURFACE_MAX or (coord_pivot_pts.y + random_shape_size.height) > HEIGHT_SURFACE_MAX or (coord_pivot_pts.x - random_shape_size.width) < WIDTH_SURFACE_MIN or (coord_pivot_pts.y - random_shape_size.height) < HEIGHT_SURFACE_MIN:
                # random a new point
                coord_pivot_pts_temp = self.random_point(nb_pts=1, only_point=True)
                if (coord_pivot_pts_temp.x + random_shape_size.width) < WIDTH_SURFACE_MAX or (coord_pivot_pts_temp.y + random_shape_size.height) < HEIGHT_SURFACE_MAX or (coord_pivot_pts_temp.x - random_shape_size.width) > WIDTH_SURFACE_MIN or (coord_pivot_pts_temp.y - random_shape_size.height) > HEIGHT_SURFACE_MIN:
                    coord_pivot_pts = coord_pivot_pts_temp
                    continue
            
            if geoshape['isometric']:
                random_shape_size = SizeRandomized(random_shape_size[1], random_shape_size[1])    
            data_randomized = [{'center_point' : coord_pivot_pts}, {'random_shape_size' : random_shape_size}]
            
            shape = eval(geoshape['type'].format('(id_pts, data_randomized)'))
            shape_objects.append(shape)
            
        return shape_objects
    
    @staticmethod
    def run_process():
        """
        This static method launch the process
        """
        print('Please, Wait process...')
        
        # -1 get shape list
        list_shapes = surface.shape_list
        # -2 get target point list
        list_target_points = surface.points_target
        # -3 statitics
        count_shapes_hited = 0
        data = []
        for id_target_point in list_target_points:
            coord_point_target_point = list_target_points[id_target_point]
            shape_list = []
            for shape in list_shapes:
                if shape.is_inside(coord_point_target_point):
                        shape.is_hited.append(id_target_point)
                        shape_list.append(shape)
            data.append(shape_list)
        
        # delete empty dict for shape did'nt have hit
        data_filter = list(filter(None, data))
        
        # get how many shapes are hited by target point
        count_shapes_hited = len(data_filter)
        count_target_point_hits = 0
        for element_shape in data_filter:

            for element in element_shape:
                #print('the shape {} hited {} times--> '.format(element, len(list_shapes[element.id].is_hited)))
                count_target_point_hits += len(list_shapes[element.id].is_hited)
                pass
        
        print('_'*50)
        print('Total shapes hited : {} / {}' .format(count_shapes_hited, surface.total_pts_shape))
        print('Mean shapes hits per point target : ', count_target_point_hits / surface.total_pts_target)
        print('_'*50 + '\n')
        print('Process Finished\n')     
      
class Rectangle():
    """
    class Rectangle: Instances to scatter on the surface.
    """
    def __init__(self, id, data):
        self.id = id
        self.type = 'Rectangle'
        self.width = data[1]['random_shape_size'][0]
        self.height = data[1]['random_shape_size'][1]
        self.center_point = data[0]['center_point']
        self.is_hited = []
        
        self.offset_x = self.width/2
        self.offset_y = self.height/2
        
        bbox = {
            'x_min': self.center_point[0] - self.offset_x,
            'x_max': self.center_point[0] + self.offset_x,
            'y_min': self.center_point[1] - self.offset_x,
            'y_max': self.center_point[1] + self.offset_x  
        }
        
        p1 = (bbox['x_min'],bbox['y_min'])
        p2 = (bbox['x_min'],bbox['y_max'])
        p3 = (bbox['x_max'],bbox['y_min'])
        p4 = (bbox['x_max'],bbox['y_max'])
                
        self.polygon = (p1,p2,p3,p4)
           
    def is_inside(self, point):
        odd = False
        # For each edge (In this case for each point of the polygon and the previous one)
        i = 0
        j = len(self.polygon) - 1
        while i < len(self.polygon) - 1:
            i = i + 1
            if (((self.polygon[i][1] > point[1]) != (self.polygon[j][1] > point[1])) and 
                (point[0] < ((self.polygon[j][0] - self.polygon[i][0]) * (point[1] - self.polygon[i][1]) / (self.polygon[j][1] - self.polygon[i][1])) + self.polygon[i][0])):
                odd = not odd       
            j = i

        return odd

class Circle():
    """
    class Circle: Instances to scatter on the surface.
    """
    def __init__(self, id, data):
        self.id = id
        self.type = 'Circle'
        self.width = data[1]['random_shape_size'][0]
        self.height = data[1]['random_shape_size'][1]
        self.center_point = data[0]['center_point']
        self.is_hited = []

        self.offset_x = self.width/2
        self.offset_y = self.height/2
        
    def is_inside(self, point):
        __radius = self.offset_x
        px = point[0]
        py = point[1]
        odd = False
        if (px - self.center_point[0])**2 + (py - self.center_point[1])**2 < __radius**2:
            odd = not odd
            
        return odd
        
class Diamond():
    """
    class Diamond: Instances to scatter on the surface.
    """
    def __init__(self, id, data):
        self.id = id
        self.type = 'Diamond' 
        self.width = data[1]['random_shape_size'][0]
        self.height = data[1]['random_shape_size'][1]
        self.center_point = data[0]['center_point']
        self.is_hited = []
        
        offset_x = self.width/2
        offset_y = self.height/2
        
        p1 = (self.center_point[0] - offset_x, self.center_point[1])
        p2 = (self.center_point[0], self.center_point[0] - offset_y)
        p3 = (self.center_point[0], self.center_point[0] + offset_y)
        p4 = (self.center_point[0] + offset_x, self.center_point[1])
                
        self.polygon = (p1,p2,p3,p4)
        
    def is_inside(self, point):
        odd = False
        i = 0
        j = len(self.polygon) - 1
        while i < len(self.polygon) - 1:
            i = i + 1
            if (((self.polygon[i][1] > point[1]) != (self.polygon[j][1] > point[1])) and 
                (point[0] < ((self.polygon[j][0] - self.polygon[i][0]) * (point[1] - self.polygon[i][1]) / (self.polygon[j][1] - self.polygon[i][1])) + self.polygon[i][0])):
                odd = not odd
            j = i
        return odd

if __name__ =='__main__':
    
    # -1 create setup
    surface = Surface()
    # -2 launch calculate process
    surface.run_process()
    
