# FUN CODE

- Have a rectangular 2D area, 1000.0 x 1000.0 float units. 
- You need to populate it with 1000 shapes, randomly selected from 3 types: rectangle, circle, diamond (diamond is symmetrical around the X and Y axis). 
- Each shape is assigned an integer ID upon creation. 
- Their sizes and positions are also selected randomly, with the following restrictions: their maximum dimension should not be smaller than 10x10 units or larger than 100x100 units, and they should be randomly but uniformly distributed inside our 1000x1000 area and not touch or cross its boundaries. 
- Once it’s done, we need to generate 1000 random 2D point samples within the area and test which shape(s) each of them hits. 
A point can hit more than one shape. Print out the statistics: how many of the 1000 shapes have been hit in total, and the median hit rate (shape hits per point sample)


# Geometric Shapes Simulation on a Surface

This Python script simulates the dispersion of geometric shapes on a given surface. Shapes are randomly generated, and the simulation analyzes how many shapes are intersected by randomly generated target points.

## Usage Instructions

1. Make sure you have Python installed on your machine.

2. Run the script using the following command in your terminal:

   ```bash
   python script_name.py


# Configuration

You can adjust certain parameters in the script according to your needs:

- Surface dimensions (HEIGHT_SURFACE_MAX, WIDTH_SURFACE_MAX).
- Total number of shape points and target points (TOTAL_SHAPE_PTS, TOTAL_TARGET_PTS).
- List of shape types with associated parameters (SHAPE_LIST).
- Minimum and maximum size of shapes (MIN_SIZE_SHAPE, MAX_SIZE_SHAPE).


## Shape Classes
- The script includes three shape classes:

- Rectangle: Defined by width, height, and a center point.
- Circle: Defined by width, height (interpreted as diameter), and a center point.
- Diamond: Defined by width, height, and a center point.

# Execution

The script generates shape points, target points, and then performs a simulation to determine how many shapes are intersected by each target point. The results are displayed at the end of the execution
